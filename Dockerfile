FROM openjdk:12-alpine
VOLUME /tmp
ADD /build/libs/*.jar theme-park-ride-gradle.jar 
EXPOSE 5000
ENTRYPOINT exec java -jar theme-park-ride-gradle.jar 
HEALTHCHECK CMD curl --fail http://localhost:5000/ride || exit 1
